BRAILLE SNN - Simulation instructions

Copy the Team_Braille_SNN/ directory to the home directory.
Open virtuoso in cadence_dir/.
The SNN is in the Braille_SNN library.
The top_level_net cell has the schematic and symbol for the network, and tb_top_level_net has the testbench.

To simulate the network, open schematic and spectre_state_1 from tb_top_level_net.
The schematic has four pressure inputs P1, P2, P3, P4 and four spiketrain outputs S1, S2, S3, S4 (representing characters A, B, C, D, respectively).
Each pressure input gets time data from a file in Team_Braille_SNN/pressure_waves/.
There are eight pressure waves in the directory, named pressure_wave_{on/off}_{1-4}.txt.
The on waves represent pressure sensor data for a dot detection, and the off waves represent data for no detection.
There are four versions of each, with randomly generated parameters.
The four source input files in the testbench schematic can be changed to model different sensor inputs to the network.
The dot patterns for each character are:

A: [on, off, off, off]
B: [on, on, off, off]
C: [on, off, on, off]
D: [on, off, on, on]

Run spectre_state1 to perform a 32 ms transient simulation.
It will plot the pressure sensor data P1-4, and the spiketrain responses for each character, S1-4.
