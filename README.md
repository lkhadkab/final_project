Folder to submit your final project. 

The folder should be the team name for instance Team_RF. 

you will need to get access to this folder as you did for mini_project. Subsequently, you will need to git clone by doing the following :

git clone https://gitlab.com/teaching_umd/enee_408d/final_project.git

Then cp your final project in here. Note it should have all the libraries that are being called in your final schematic and layout. 


For pushing I have simiplified the process by creating a shell script. Once you have all the folders call this file as:
 
source git_script.sh

