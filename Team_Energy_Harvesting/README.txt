The FULL_SCHEMATIC folder contains the complete schematic design, but a full simulation can not be ran on it because it
would take too long. Instead the simulation is split into two parts: power generation and sensor reading.

The sensor reading simulation is done by the circuit in FULL_DESIGN and is mean to run on a 5u transient simulation, where
the sensor reading is given by vin. The input file input3.txt is used as an input voltage to the circuit. Changing values of 
Vin give different output bits of the ADC. See the design report for more information on the simulation

 