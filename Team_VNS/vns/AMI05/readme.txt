To include the AMI05 models for nmos4 and pmos4, enter in:

Analog Design Environement -> Setup -> Model Libraries-> Browse
 to AMI05/tt.include which points to the spectre.tt file in that directory

.

tt.include simply points to the latest NMOS and PMOS model files for the ON .5um process V37P.m (from run V37p as of Jan 2014).

This directory also includes the following files:

Elecparameter = which gives the measured electrical parameter for the V37p run.

FF.m = Fast/Fast corner model which models NMOS and PMOS acting as fast transistors.

FS.m = Fast(n)/Slow(p) corner model which models NMOS and PMOS effectively  functioning at different speeds.

SF.m = Slow(n)/Fast(p) corner model which models NMOS and PMOS effectively functioning at different speeds.

SS.m = Slow/Slow corner model which models NMOS and PMOS acting as slow transistors.

Note that the corner models are not recent.

Also included is old_models.m which includes both nmos and pmos models from the same process that were extraceted 10 years ago.