/************************************************************************
 * FileName:  cp_config.h
 * Location:  design/checkplus/.
 * 
 * This file is used to customize global variables 
 *
 * Defaults are located in the files:
 * $CDS_INST_DIR/tools/checkplus/composer/rules_include/cp_config.h
 * where:
 * $CDS_INST_DIR = The Cadence tools installation directory
 *
 *************************************************************************/

