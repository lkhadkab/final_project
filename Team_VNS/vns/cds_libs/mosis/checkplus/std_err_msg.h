/************************************************************************
 * FileName:  std_err_msg.h
 * Location:  design/checkplus/.
 *
 * This file is used to customize any rules that contain variable
 * parameters.  To determine if a rule has variable paramters check
 * the online help for the specific rule.
 *
 * Defaults are located in the files:
 * $CDS_INST_DIR/tools/checkplus/composer/rules_include/$RULEFILE.h
 * where:
 * $CDS_INST_DIR = The Cadence tools installation directory
 * $RULEFILE = The name of the rule file where the rule is located.
 *
 *************************************************************************/

