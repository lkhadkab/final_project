
/****************************************************/
 LIBRARY = "mosis"
 CELL    = "pdiode"
/****************************************************/

let( ( libId cellId cdfId )
    unless( cellId = ddGetObj( LIBRARY CELL )
        error( "Could not get cell %s." CELL )
    )
    when( cdfId = cdfGetBaseCellCDF( cellId )
        cdfDeleteCDF( cdfId )
    )
    cdfId  = cdfCreateBaseCellCDF( cellId )

    ;;; Parameters
    cdfCreateParam( cdfId
        ?name           "c"
        ?prompt         "Capacitance"
        ?units          "capacitance"
        ?defValue       "1p"
        ?type           "string"
        ?parseAsNumber  "yes"
        ?parseAsCEL     "yes"
    )
    cdfCreateParam( cdfId
        ?name           "ic"
        ?prompt         "Initial condition"
        ?units          "voltage"
        ?defValue       "none"
        ?type           "string"
        ?parseAsNumber  "yes"
        ?parseAsCEL     "yes"
    )
    cdfCreateParam( cdfId
        ?name           "source"
        ?prompt         "Source component"
        ?type           "boolean"
    )
    cdfCreateParam( cdfId
        ?name           "modFile"
        ?prompt         "Model file name"
        ?defValue       "none"
        ?type           "string"
        ?parseAsCEL     "yes"
    )
    cdfCreateParam( cdfId
        ?name           "m"
        ?prompt         "Multiplier"
        ?defValue       "1"
        ?type           "string"
        ?parseAsNumber  "yes"
        ?parseAsCEL     "yes"
    )

    ;;; Simulator Information
    cdfId->simInfo = list( nil )
    cdfId->simInfo->auLvs = '( nil
        netlistProcedure  ansLvsCompPrim
        parameters        (c)
        componentName     cap
        termOrder         (PLUS MINUS)
        propMapping       (nil C c)
        permuteRule       "(p PLUS MINUS)"
        namePrefix        "C"
    )
    cdfId->simInfo->cdsSpice = '( nil
        netlistProcedure  ansSpiceCompPrim
        parameters        (c ic)
        componentName     cap
        termOrder         (PLUS MINUS)
        namePrefix        "C"
        current           component
        dcSens            nil
        acSens            t
    )
    cdfId->simInfo->hspiceS = '( nil
        netlistProcedure  ansSpiceCompPrim
        parameters        (c ic)
        componentName     cap
        termOrder         (PLUS MINUS)
        namePrefix        "C"
        current           component
        dcSens            nil
        acSens            t
    )
    cdfId->simInfo->libra = '( nil
        netlistProcedure  ansLibraCompPrim
        parameters        (c source)
        componentName     cap
        termOrder         (PLUS MINUS)
        namePrefix        ""
    )
    cdfId->simInfo->spectreS = '( nil
        termMapping       (nil PLUS \1 MINUS \2)
        netlistProcedure  ansSpiceCompPrim
        parameters        (c modFile m ic)
        componentName     cap
        termOrder         (PLUS MINUS)
        namePrefix        "C"
        current           port
        dcSens            nil
        acSens            t
    )
    cdfId->simInfo->switcap = '( nil
        netlistProcedure  ansSwitcapCompPrim
        parameters        (c)
        componentName     cap
        termOrder         (PLUS MINUS)
        namePrefix        ""
    )
    cdfId->simInfo->watscad = '( nil
        netlistProcedure  ansWatscadCompPrim
        parameters        (c)
        componentName     cap
        termOrder         (PLUS MINUS)
        namePrefix        ""
    )

    ;;; Properties
    cdfId->formInitProc            = ""
    cdfId->doneProc                = ""
    cdfId->buttonFieldWidth        = 340
    cdfId->fieldHeight             = 35
    cdfId->fieldWidth              = 350
    cdfId->promptWidth             = 175
    cdfId->opPointLabelSet         = "i"
    cdfId->paramLabelSet           = "c ic"
    cdfSaveCDF( cdfId )
)
