* T36S SPICE BSIM3 VERSION 3.1 PARAMETERS

* SPICE 3f5 Level 8, Star-HSPICE Level 49, UTMOST Level 8

* DATE: Aug 13/03
* LOT: T36S                  WAF: 1101
* Temperature_parameters=Default
*.MODEL nmos4 bsim3v3 type=n  
*+VERSION = 3.1            TNOM    = 27             TOX     = 1.41E-8
*+XJ      = 1.5E-7         NCH     = 1.7E17         VTH0    = 0.6461285
*+K1      = 0.9152455      K2      = -0.105898      K3      = 26.1007653
*+K3B     = -8.3258109     W0      = 1E-8           NLX     = 1E-9
*+DVT0W   = 0              DVT1W   = 0              DVT2W   = 0
*+DVT0    = 3.7861788      DVT1    = 0.3717718      DVT2    = -0.0830195
*+U0      = 442.2002499    UA      = 1E-13          UB      = 1.185519E-18
*+UC      = 6.470959E-12   VSAT    = 1.727533E5     A0      = 0.6072333
*+AGS     = 0.1377586      B0      = 2.667083E-6    B1      = 5E-6
*+KETA    = -2.897119E-3   A1      = 3.028408E-4    A2      = 0.345619
*+RDSW    = 1.077249E3     PRWG    = 0.1399237      PRWB    = 0.0595509
*+WR      = 1              WINT    = 2.208544E-7    LINT    = 4.960111E-8
*+XL      = 0              XW      = 2E-7	    DWG     = -1.893724E-9
*+DWB     = 5.47692E-8     VOFF    = 0              NFACTOR = 0.472999
*+CIT     = 0              CDSC    = 2.4E-4         CDSCD   = 0
*+CDSCB   = 0              ETA0    = 2.181479E-3    ETAB    = -6.299339E-4
*+DSUB    = 0.0689754      PCLM    = 2.4881989      PDIBLC1 = 0.9941032
*+PDIBLC2 = 2.298113E-3    PDIBLCB = -0.0180618     DROUT   = 0.8838803
*+PSCBE1  = 6.445443E8     PSCBE2  = 2.598335E-4    PVAG    = 0
*+DELTA   = 0.01           RSH     = 81.8           MOBMOD  = 1
*+PRT     = 0              UTE     = -1.5           KT1     = -0.11
*+KT1L    = 0              KT2     = 0.022          UA1     = 4.31E-9
*+UB1     = -7.61E-18      UC1     = -5.6E-11       AT      = 3.3E4
*+WL      = 0              WLN     = 1              WW      = 0
*+WWN     = 1              WWL     = 0              LL      = 0
*+LLN     = 1              LW      = 0              LWN     = 1
*+LWL     = 0              CAPMOD  = 2              XPART   = 0.5
*+CGDO    = 2.01E-10       CGSO    = 2.01E-10       CGBO    = 1E-9
*+CJ      = 4.227962E-4    PB      = 0.9113851      MJ      = 0.4296861
*+CJSW    = 2.925306E-10   PBSW    = 0.8            MJSW    = 0.170165
*+CJSWG   = 1.64E-10       PBSWG   = 0.8            MJSWG   = 0.170165
*+CF      = 0              PVTH0   = 0.1375778      PRDSW   = -174.8924404
*+PK2     = -0.0194887     WKETA   = -0.0237035     LKETA   = 0.0205439       
**
.MODEL pmos4 bsim3v3 type=p 
+VERSION = 3.1            TNOM    = 27             TOX     = 1.41E-8
+XJ      = 1.5E-7         NCH     = 1.7E17         VTH0    = -0.9213369
+K1      = 0.5309886      K2      = 0.0157077      K3      = 3.9841467
+K3B     = -0.8977975     W0      = 1E-8           NLX     = 1E-9
+DVT0W   = 0              DVT1W   = 0              DVT2W   = 0
+DVT0    = 2.8313335      DVT1    = 0.4250351      DVT2    = -0.055987
+U0      = 209.2163727    UA      = 2.878347E-9    UB      = 1.397451E-21
+UC      = -6.27575E-11   VSAT    = 2E5            A0      = 0.9102666
+AGS     = 0.1449591      B0      = 1.211218E-6    B1      = 5E-6
+KETA    = -1.616658E-3   A1      = 0              A2      = 0.3
+RDSW    = 2.088968E3     PRWG    = 2.930817E-4    PRWB    = -0.0963307
+WR      = 1              WINT    = 3.279186E-7    LINT    = 6.251424E-8
+XL      = 0              XW      = 2E-7           DWG     = -2.846864E-8
+DWB     = 1.437983E-8    VOFF    = -0.0130604     NFACTOR = 1.0218439
+CIT     = 0              CDSC    = 2.4E-4         CDSCD   = 0
+CDSCB   = 0              ETA0    = 0.1086822      ETAB    = -0.0677706
+DSUB    = 0.8682467      PCLM    = 2.168708       PDIBLC1 = 0.112826
+PDIBLC2 = 3.525757E-3    PDIBLCB = -0.059791      DROUT   = 0.3016682
+PSCBE1  = 5.091678E9     PSCBE2  = 5E-10          PVAG    = 0.0256592
+DELTA   = 0.01           RSH     = 102            MOBMOD  = 1
+PRT     = 0              UTE     = -1.5           KT1     = -0.11
+KT1L    = 0              KT2     = 0.022          UA1     = 4.31E-9
+UB1     = -7.61E-18      UC1     = -5.6E-11       AT      = 3.3E4
+WL      = 0              WLN     = 1              WW      = 0
+WWN     = 1              WWL     = 0              LL      = 0
+LLN     = 1              LW      = 0              LWN     = 1
+LWL     = 0              CAPMOD  = 2              XPART   = 0.5
+CGDO    = 2.62E-10       CGSO    = 2.62E-10       CGBO    = 1E-9
+CJ      = 7.247826E-4    PB      = 0.9541788      MJ      = 0.4958349
+CJSW    = 2.613323E-10   PBSW    = 0.99           MJSW    = 0.2516611
+CJSWG   = 6.4E-11        PBSWG   = 0.99           MJSWG   = 0.2516611
+CF      = 0              PVTH0   = 5.98016E-3     PRDSW   = 14.8598424
+PK2     = 3.73981E-3     WKETA   = 3.391823E-3    LKETA   = -8.97011E-3     
*







.MODEL   bip1    PNP      (   
+ IS     = 0.78e-15	   RC	  = 75		   
+ BF     = 225             CJE	  = 0.5e-12         VAF    = 150 
+ VJE    = 0.65            ISE    = 0.15e-15        MJE    = 0.4
+ NE     = 1.28            TF     = 40e-9           BR     = 1
+ CJC    = 0.5e-12         VAR    = 38              VJC    = 0.65
+ ISC    = 1.5e-15         MJC    = 0.4             RB     = 100
+ TR     = 2e-6	           RE     = 5               CJS    = 0
+ VJS    = 0.65		   MJS    = 0   )

*




* mosis ami05 t48x spice bsim3 version 3.1 parameters
* spice 3f5 level 8, star-hspice level 49, utmost level 8

* date: oct 18/04
* lot: t48x                  waf: 0104
* temperature_parameters=default
.Model nmos4 bsim3v3 type=n
+version = 3.1            tnom    = 27             tox     = 1.4e-8
+xj      = 1.5e-7         nch     = 1.7e17         vth0    = 0.6429327
+k1      = 0.9000289      k2      = -0.107613      k3      = 25.2759275
+k3b     = -8.3994254     w0      = 1e-8           nlx     = 1e-9
+dvt0w   = 0              dvt1w   = 0              dvt2w   = 0
+dvt0    = 2.8022093      dvt1    = 0.444009       dvt2    = -0.1416727
+u0      = 445.5561298    ua      = 1e-13          ub      = 1.362333e-18
+uc      = 7.096474e-12   vsat    = 1.636634e5     a0      = 0.6036695
+ags     = 0.1337541      b0      = 2.628741e-6    b1      = 5e-6
+keta    = -1.772978e-3   a1      = 2.389998e-4    a2      = 0.3630421
+rdsw    = 1.18102e3      prwg    = 0.078332       prwb    = 0.0419119
+wr      = 1              wint    = 2.434318e-7    lint    = 7.757158e-8
+xl      = 0	          xw      = 2e-7              dwg     = -7.96929e-9
+dwb     = 4.850872e-8    voff    = 0              nfactor = 0.9169218
+cit     = 0              cdsc    = 2.4e-4         cdscd   = 0
+cdscb   = 0              eta0    = 3.75392e-3     etab    = -5.591826e-5
+dsub    = 0.1525772      pclm    = 2.5213573      pdiblc1 = 1
+pdiblc2 = 3.431795e-3    pdiblcb = -0.0496671     drout   = 1.0119109
+pscbe1  = 6.156161e8     pscbe2  = 1.167827e-4    pvag    = 0
+delta   = 0.01           rsh     = 83.2           mobmod  = 1
+prt     = 0              ute     = -1.5           kt1     = -0.11
+kt1l    = 0              kt2     = 0.022          ua1     = 4.31e-9
+ub1     = -7.61e-18      uc1     = -5.6e-11       at      = 3.3e4
+wl      = 0              wln     = 1              ww      = 0
+wwn     = 1              wwl     = 0              ll      = 0
+lln     = 1              lw      = 0              lwn     = 1
+lwl     = 0              capmod  = 2              xpart   = 0.5
+cgdo    = 1.98e-10       cgso    = 1.98e-10       cgbo    = 1e-9
+cj      = 4.213189e-4    pb      = 0.9096858      mj      = 0.4299892
+cjsw    = 2.868393e-10   pbsw    = 0.8            mjsw    = 0.1691219
+cjswg   = 1.64e-10       pbswg   = 0.8            mjswg   = 0.1691219
+cf      = 0              pvth0   = 0.0450737      prdsw   = 317.3628776
+pk2     = -0.0248633     wketa   = -0.0238492     lketa   = -4.362509e-5    
*



