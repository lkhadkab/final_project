*V3BM SPICE BSIM3 VERSION 3.1 PARAMETERS

*SPICE 3f5 Level 8, Star-HSPICE Level 49, UTMOST Level 8

* DATE: May  1/14
* LOT: v3bm                  WAF: 1003
* Temperature_parameters=Default
*.MODEL CMOSN NMOS (                                LEVEL   = 49
.MODEL nmos4 bsim3v3 type=n  
+VERSION = 3.1            TNOM    = 27             TOX     = 1.4E-8
+XJ      = 1.5E-7         NCH     = 1.7E17         VTH0    = 0.6155718
+K1      = 0.927165       K2      = -0.1083911     K3      = 22.8103193
+K3B     = -9.5490578     W0      = 1.011931E-8    NLX     = 2.526408E-9
+DVT0W   = 0              DVT1W   = 0              DVT2W   = 0
+DVT0    = 0.8806411      DVT1    = 0.3803297      DVT2    = -0.2060183
+U0      = 455.1710634    UA      = 1.041298E-13   UB      = 1.579609E-18
+UC      = 1.13123E-11    VSAT    = 1.983149E5     A0      = 0.6022739
+AGS     = 0.1382025      B0      = 1.831247E-6    B1      = 5E-6
+KETA    = -4.902055E-3   A1      = 5.595408E-5    A2      = 0.3
+RDSW    = 1.062E3        PRWG    = 0.0798664      PRWB    = -0.0174716
+WR      = 1              WINT    = 2.216754E-7    LINT    = 8.043861E-8
+XL      = 1E-7           XW      = 0              DWG     = -8.322467E-9
+DWB     = 4.618635E-8    VOFF    = -1.506501E-4   NFACTOR = 1.1555205
+CIT     = 0              CDSC    = 2.4E-4         CDSCD   = 0
+CDSCB   = 0              ETA0    = 2.035354E-3    ETAB    = -4.669429E-4
+DSUB    = 0.0702864      PCLM    = 2.0810693      PDIBLC1 = 8.593454E-6
+PDIBLC2 = 2.091439E-3    PDIBLCB = 0.1197245      DROUT   = 0
+PSCBE1  = 2.246943E8     PSCBE2  = 9.969607E-8    PVAG    = 0
+DELTA   = 0.01           RSH     = 79.4           MOBMOD  = 1
+PRT     = 0              UTE     = -1.5           KT1     = -0.11
+KT1L    = 0              KT2     = 0.022          UA1     = 4.31E-9
+UB1     = -7.61E-18      UC1     = -5.6E-11       AT      = 3.3E4
+WL      = 0              WLN     = 1              WW      = 0
+WWN     = 1              WWL     = 0              LL      = 0
+LLN     = 1              LW      = 0              LWN     = 1
+LWL     = 0              CAPMOD  = 2              XPART   = 0.5
+CGDO    = 1.88E-10       CGSO    = 1.88E-10       CGBO    = 1E-9
+CJ      = 4.176198E-4    PB      = 0.8350587      MJ      = 0.4275656
+CJSW    = 3.668183E-10   PBSW    = 0.8            MJSW    = 0.2085947
+CJSWG   = 1.64E-10       PBSWG   = 0.8            MJSWG   = 0.2019414
+CF      = 0              PVTH0   = -5.305536E-3   PRDSW   = 207.8016369
+PK2     = -0.0810173     WKETA   = -1.413082E-3   LKETA   = -2.178234E-3    )
*
*.MODEL CMOSP PMOS (                                LEVEL   = 49
.MODEL pmos4 bsim3v3 type=p 
+VERSION = 3.1            TNOM    = 27             TOX     = 1.4E-8
+XJ      = 1.5E-7         NCH     = 1.7E17         VTH0    = -0.9152268
+K1      = 0.553472       K2      = 7.871921E-3    K3      = 1.5
+K3B     = 0.0273558      W0      = 3.3916E-7      NLX     = 9.640668E-9
+DVT0W   = 0              DVT1W   = 0              DVT2W   = 0
+DVT0    = 0.818189       DVT1    = 0.3110325      DVT2    = -0.0916704
+U0      = 201.3603195    UA      = 2.48572E-9     UB      = 1.005454E-21
+UC      = -1E-10         VSAT    = 1.047533E5     A0      = 0.7091485
+AGS     = 0.1292184      B0      = 8.124014E-7    B1      = 1.081483E-8
+KETA    = -4.865785E-3   A1      = 3.62251E-4     A2      = 0.6482228
+RDSW    = 2.946814E3     PRWG    = -0.0219441     PRWB    = -0.0584559
+WR      = 1              WINT    = 2.208839E-7    LINT    = 1.111912E-7
+XL      = 1E-7           XW      = 0              DWG     = -2.726688E-9
+DWB     = -2.015846E-8   VOFF    = -0.0692697     NFACTOR = 0.7808767
+CIT     = 0              CDSC    = 2.4E-4         CDSCD   = 0
+CDSCB   = 0              ETA0    = 4.415482E-3    ETAB    = -0.04
+DSUB    = 0.85           PCLM    = 2.5353837      PDIBLC1 = 0.0622214
+PDIBLC2 = 4.508339E-3    PDIBLCB = -0.0220558     DROUT   = 0.2510332
+PSCBE1  = 7.325064E9     PSCBE2  = 7.921606E-9    PVAG    = 4.557354E-4
+DELTA   = 0.01           RSH     = 101.5          MOBMOD  = 1
+PRT     = 0              UTE     = -1.5           KT1     = -0.11
+KT1L    = 0              KT2     = 0.022          UA1     = 4.31E-9
+UB1     = -7.61E-18      UC1     = -5.6E-11       AT      = 3.3E4
+WL      = 0              WLN     = 1              WW      = 0
+WWN     = 1              WWL     = 0              LL      = 0
+LLN     = 1              LW      = 0              LWN     = 1
+LWL     = 0              CAPMOD  = 2              XPART   = 0.5
+CGDO    = 2.44E-10       CGSO    = 2.44E-10       CGBO    = 1E-9
+CJ      = 7.183583E-4    PB      = 0.865527       MJ      = 0.4904275
+CJSW    = 2.447569E-10   PBSW    = 0.8            MJSW    = 0.1910967
+CJSWG   = 6.4E-11        PBSWG   = 0.8            MJSWG   = 0.2261452
+CF      = 0              PVTH0   = 5.98016E-3     PRDSW   = 14.8598424
+PK2     = 3.73981E-3     WKETA   = 0.0123545      LKETA   = -0.0149634      )
*
