#!/bin/bash
find . -type f -name "*cdslck*" -exec rm {} \;

# Check if we're in a git repository
if ! git rev-parse --git-dir > /dev/null 2>&1; then
    echo "Not a git repository. Exiting..."
    exit 1
fi
git pull
# Check if there are any changes to pull
if [[ -n $(git status -uno --porcelain) ]]; then
    # Changes exist, pull before pushing
    git pull
fi

# Add all changes and commit
read -p "Enter commit message: " commit_message
git add --all
git commit -m "$commit_message"

# Push to remote
git push origin main

